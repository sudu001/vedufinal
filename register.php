
<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Headers; 

$login = new Login();



?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Registration</title>
	<?php include_once Config::path()->INCLUDE_PATH.'/fronthead.php'; ?>
</head>

<body data-ng-app="">
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontheader.php'; ?>
	<section class="tz-register">
			<div class="log-in-pop">
				<div class="log-in-pop-left">
					<h1>Hello... <span>{{ name1 }}</span></h1>
					<p>&nbsp;Don't have an account? Create your account. It's take less then a minutes</p>
					<h4>Login with social media</h4>
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
						</li>
						<li><a href="#"><i class="fa fa-google"></i> Google+</a>
						</li>
						<li><a href="#"><i class="fa fa-twitter"></i> Twitter</a>
						</li>
					</ul>
				</div>
				<div class="log-in-pop-right panel-body">
				<!--	<a href="register.html#" class="pop-close" data-dismiss="modal"><img src="http://rn53themes.net/themes/demo/directory/images/cancel.png" alt="" />
					</a>-->
					<h4>Create an Account</h4>
					<p>Don't have an account? Create your account. It's take less then a minutes</p>
					
					<form class="s12" name="frmregister" id="frmregister" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
						<!--<div>
							<div class="input-field s12">
								<select  name="role"  id="role"  class="validate" required>
                            <option value="">----Select Role----</option>
								<?php 
                              //  $login->getRole();
                                ?>
                                </select>
								
							</div>
						</div>-->
						<div>
							<div class="input-field s12">
								<input type="radio"  name="role" id="role" value="Students">
								<label for="Students">Students</label>
							&nbsp;&nbsp;&nbsp;&nbsp;	<input type="radio"  name="role" id="role" value="School">
								<label for="School">School</label>
						
							</div>
						</div>	
						<div>
							<div class="input-field s12">
								<input type="text" data-ng-model="name1" name="name" id="name" class="validate" required>
								<label>User name</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="email" class="validate" name="email" id="email" required>
								<label>Email id</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="password" class="validate" id="password" name="password" required>
								<label>Password</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="password" class="validate" id="confirmpassword" name="confirmpassword" required>
								<label>Confirm password</label>
							</div>
							
						</div>
						<div>
							<div class="input-field s4">
								<input type="submit"  name="signUp" value="Register" id="signUp" class="waves-effect waves-light log-in-btn"> </div>
						</div>
						<div>
							<div class="input-field s12"> <a href="/login">Are you a already member ? Login</a> </div>
						</div>
					</form>
				</div>
				<div id="tabs-2" style="display:none;">
					
				</div>
			</div>
	</section>
	<!--MOBILE APP-->
	<!--<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="register.html#"><img src="images/android.png" alt="" /> </a>
					<a href="register.html#"><img src="images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> -->
	<!--FOOTER SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontfooter.php'; ?>
	<!--COPY RIGHTS-->
	<?php include_once Config::path()->INCLUDE_PATH.'/copyright.php'; ?>
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
		<?php include_once Config::path()->INCLUDE_PATH.'/frontscript.php'; ?>
	    <script type="text/javascript">
	    	
	  <link href="<?php echo Config::path()->ASSETSFRONT ;?>/css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="<?php echo Config::path()->ASSETSFRONT ;?>/js/jquery.alerts.js"></script>
	    <script type="text/javascript">
	
         $(document).ready(function(){
	    $("#signUp").click(function(){
		var name = $("#name").val();
		var email = $("#email").val();
		var password = $("#password").val();
		var confirmpassword = $("#confirmpassword").val();
//		alert("serching suggestion name is"+name); 
//		jAlert('Please fill the above entries.', 'Alert');
if(password != confirmpassword)
	{
	alert("Confirm Password mismatched! Try again");
	document.forms['frmregister']['confirmpassword'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"processsignup.php",
         data:$("#frmregister").serialize(),
         success: function(result){
            $( ".panel-body" ).hide(result);
            $( "#tabs-2" ).html(result).show();   
        }
       });

     return false;
}
	  });
    });
	</script>
</body>

</html>